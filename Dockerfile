FROM node

COPY . /opt/flatris

WORKDIR /opt/flatris

RUN  yarn install 
#RUN yarn test 
RUN yarn build

CMD yarn start

EXPOSE 3000

